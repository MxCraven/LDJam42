extends RichTextLabel

func _ready():
	self.push_align(2)

func _process(delta):
	var jumps = get_node("/root/global").total_jumps
	self.clear()
	if jumps <= 5:
		self.push_color(Color(1.0, 0.0, 0.0))
	self.append_bbcode("[center] Jumps Left! %s" % str(jumps))
	
