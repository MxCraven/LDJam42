extends Control

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass


func _on_RetryButton_pressed():
	get_tree().change_scene(get_node("/root/global").current_level_path)
	



func _on_QuitButton_pressed():
	get_tree().quit()
