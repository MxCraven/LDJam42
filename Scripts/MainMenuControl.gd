extends Control

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	get_node("/root/global").jumps_saved = 0

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass


func _on_GoButton_pressed():
	print("Pressed")
	get_tree().change_scene("res://Scenes/Level1.tscn")

func _on_QuitButton_pressed():
	get_tree().quit()
