extends Node

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	get_node("/root/global").total_jumps = 4
	get_node("/root/global").current_level_path = "res://Scenes/Level4.tscn"
	get_node("/root/global").next_level_path = "res://Scenes/Level5.tscn"

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
