extends KinematicBody2D

const RUN_SPEED = 5000
const JUMP_SPEED = -11000
const GRAVITY = 20000

var velocity = Vector2()

var left_sprite = preload("res://Sprites/PlayerLeft.png")
var right_sprite = preload("res://Sprites/PlayerRight.png")
var front_sprite = preload("res://Sprites/PlayerFront.png")

func get_input():
	velocity.x = 0
	var right = Input.is_action_pressed('right')
	var left = Input.is_action_pressed('left')
	var jump = Input.is_action_just_pressed('up')
	
	if is_on_floor() and jump:
		if get_node("/root/global").total_jumps > 0:
			get_node("/root/global").total_jumps -= 1
			velocity.y = JUMP_SPEED
		else:
			get_tree().change_scene("res://Scenes/YouDied.tscn")
	if right:
		velocity.x += RUN_SPEED
		get_node("Sprite").texture = right_sprite
	if left:
		velocity.x -= RUN_SPEED
		get_node("Sprite").texture = left_sprite
	if !left && !right:
		get_node("Sprite").texture = front_sprite
	
func _physics_process(delta):
	velocity.y += GRAVITY * delta
	get_input()
	velocity = move_and_slide(velocity, Vector2(0, -1))


func _on_Area2D_area_entered(area):
	print("Something entered")
